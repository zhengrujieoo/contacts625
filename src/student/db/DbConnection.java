package student.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;
public class DbConnection {
	private static final String driverName="com.microsoft.sqlserver.jdbc.SQLServerDriver";
	private static final String dbURL="jdbc:sqlserver://localhost:1433;DatabaseName=contacts625;;integratedSecurity=true;";
	private static final String userName="sa";
    private static final String userPwd="123456";
//	private static final String USER = "sa";
//	private static final String PASSWORD = "123456";
//	private static final String PARAMETER8 = "characterEncoding=utf8&useSSL=false&serverTimezone=GMT%2B8"; 
//	private static final String URL = "jdbc:SQL Server://localhost:3306/contacts625?"+ PARAMETER8;  
	private static Connection con;
	public static Connection getConnection() {
		try {
			
			//Class.forName("com.mysql.cj.jdbc.Driver");
			Class.forName(driverName);
			if (con == null || con.isClosed()) { 
				//con = DriverManager.getConnection(URL,USER, PASSWORD);
				con = DriverManager.getConnection(dbURL,userName, userPwd);
			} else {
				return con;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return con;
	
	}
	public static void close(PreparedStatement ps,ResultSet rs) {
		try {
			if (rs != null) {
				rs.close();
				rs = null; 
			}
			if (ps != null) {
				ps.close();
				ps = null;
			}
			if (con != null) {
				con.close();
				con = null; 
			}		
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	


}
