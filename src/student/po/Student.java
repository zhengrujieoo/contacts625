package student.po;

import java.io.Serializable;
import java.util.Date;//连接数据库，date怎么转换


public class Student {
	
	//private static final long serialVersionUID = 1L;
	private int studentId;
	private String name;
	private Date birthday;
	private String phoneNumber;
	private Room room;
	public Student() {
		super();
	}
	
	public Student(int studentId, String name, Date birthday, String phoneNumber, Room room) {
		super();
		this.studentId = studentId;
		this.name = name;
		this.birthday = birthday;
		this.phoneNumber = phoneNumber;
		this.room = room;
	}

	public int getStudentId() {
		return studentId;
	}
	public void setStudentId(int studentId) {
		this.studentId = studentId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getBirthday() {
		return birthday;
	}
	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public Room getRoom() {
		return room;
	}
	public void setMajorClass(Room room) {
		this.room = room;
	}

	@Override
	public String toString() {
		return "Student [studentId=" + studentId + ", name=" + name +", phoneNumber=" + phoneNumber +", birthday=" + birthday +", room=" + room + "]";
	}
	
	

}
