package student.po;

public class Room {
	private int roomNo;
	private String roomName;
	
	public int getRoomNo() {
		return roomNo;
	}

	public void setRoomNo(int roomNo) {
		this.roomNo = roomNo;
	}
	public String getRoomName() {
		return roomName;
	}
	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}
	public Room(int roomNo,String roomName) {
		super();
		this.roomNo = roomNo;
		this.roomName = roomName;
	}


	@Override
	public String toString() {
		return "Room [roomNo=" + roomNo + ", roomName=" + roomName + "]";
	}
}
