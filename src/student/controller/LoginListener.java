package student.controller;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import student.po.Room;
import student.po.Student;
import student.service.RoomService;
import student.service.StudentService;
import student.view.ContactsDialog;
import student.view.MainView;


public class LoginListener implements ActionListener {
	private MainView mainView;
	private ContactsDialog contactsDialog;
	private StudentService studentService;
	
	
	/**
	 *事件处理
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
	
			if("addStudentDialog".equals(e.getActionCommand())) {
				contactsDialog = new ContactsDialog(mainView,"添加学生",null,null);
			}	
			else if ("addPost".equals(e.getActionCommand())) {
				Student newStudent = this.getStudentInstance();
				if(newStudent!=null) {
					studentService.add(newStudent);
					JOptionPane.showMessageDialog(mainView, "添加学号为"+newStudent.getStudentId()+"的学生成功");
					contactsDialog.dispose();
					mainView.refresh(studentService.findAll());
				}else {
					JOptionPane.showMessageDialog(mainView, "添加学生失败！");
				}
				
			}
			
			else if("searchStudentDialog".equals(e.getActionCommand())) { 
				String str = JOptionPane.showInputDialog(mainView,"输入学生学号");
				
				//for(Student student : studentService.findAll()) {
					//studentList.add(student);
				if(str != null && !"".equals(str)) { 
					if(str.matches("^[0-9]*$")){
						int studentId = Integer.parseInt(str);
						Student student = new Student();
						student.setStudentId(studentId);
						List<Student> result = studentService.get(studentId); 
						mainView.showSearchResult(result);
			}
					else {
						JOptionPane.showMessageDialog(mainView, "输入的不是数字","输入错误",JOptionPane.ERROR_MESSAGE);
					}
				}
			}
			
			//打开修改对话框
			else if("updateStudentDialog".equals(e.getActionCommand())) { 
				int selRow = mainView.getSelRow();
				if(selRow < 0) {
					JOptionPane.showMessageDialog(mainView, "请选择一行数据！");
				}else {
					Student student = studentService.findAll().get(mainView.getSelRow());
					Room room = student.getRoom();
					contactsDialog = new ContactsDialog(mainView,"修改图书",student,room);
				}
			}
			
			 //修改提交
			else if("updatePost".equals(e.getActionCommand())) { 
				Student updatedStudent = this.getStudentInstance();
				if(updatedStudent != null) {
					studentService.update(mainView.getSelRow(), updatedStudent);
					JOptionPane.showMessageDialog(mainView, updatedStudent.getStudentId()+"号学生信息修改成功！");
					contactsDialog.dispose();
					mainView.refresh(studentService.findAll());
				}else {
					JOptionPane.showMessageDialog(mainView, "更新信息失败！");
				}
				
			}
			 //删除图书操作
			else if("deleteStudent".equals(e.getActionCommand())) {
				int selRow = mainView.getSelRow();
				if(selRow <  0) {
					JOptionPane.showMessageDialog(mainView, "请选择一行数据！");
				}else {
					
					Student student = studentService.findAll().get(mainView.getSelRow());
					studentService.delete(student.getStudentId());
					JOptionPane.showMessageDialog(mainView, "删除"+student.getStudentId()+"号学生成功！");
					mainView.refresh(studentService.findAll());
					selRow = -1;
				
				}
			}
			
			//刷新图书列表窗口
			else if("showStudentDialog".equals(e.getActionCommand())) {
				mainView.refresh(studentService.findAll());
			}
			else if("refreshMainView".equals(e.getActionCommand())) {
				mainView.refresh(studentService.findAll());
			}
		}
		/**
		 * 获取图书vo实例
		 * Book为dialog传递的vo对象
		 * @return book
		 */
		public Room getRoomInstance() {
			
			String roomName = null;
			int roomNo = 0;
			Room room = null;
			String roomNoInput = contactsDialog.getRoomNoTextField().getText().trim();
			
			if(roomNoInput != null && !"".equals(roomNoInput)) {
				if(roomNoInput.matches("^[0-9]*$")){
					roomNo = Integer.parseInt(roomNoInput);
				}
				else {
					JOptionPane.showMessageDialog(mainView, "宿舍号必须是整数类型","输入错误",JOptionPane.ERROR_MESSAGE);
				}
			}else {
				JOptionPane.showMessageDialog(mainView, "宿舍号不能为空","输入错误",JOptionPane.ERROR_MESSAGE);
			}
			String roomNameInput = contactsDialog.getNameTextField().getText().trim();
			if(roomNameInput != null && !"".equals(roomNameInput)) {
				roomName = roomNameInput;
			}else {
				JOptionPane.showMessageDialog(mainView, "名称不能为空","输入错误",JOptionPane.ERROR_MESSAGE);
			}
		
			if(roomNo > 0 && roomName!=null) {
				room = new Room(roomNo,roomName);
			}
			return room;
		}
		public Student getStudentInstance() {
			int studentId = 0;
			String name = null;
			Date birthday = null;
			String phoneNumber = null;
			Student student = null;
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-mm-dd");
			String studentIdInput = contactsDialog.getStudentIdTextField().getText().trim();
			if(studentIdInput != null && !"".equals(studentIdInput)) {
				if(studentIdInput.matches("^[0-9]*$")){
					studentId = Integer.parseInt(studentIdInput);
				}
				else {
					JOptionPane.showMessageDialog(mainView, "学号必须是整数类型","输入错误",JOptionPane.ERROR_MESSAGE);
				}
			}else {
				JOptionPane.showMessageDialog(mainView, "学号不能为空","输入错误",JOptionPane.ERROR_MESSAGE);
			}
			String nameInput = contactsDialog.getNameTextField().getText().trim();
			if(nameInput != null && !"".equals(nameInput)) {
				name = nameInput;
			}else {
				JOptionPane.showMessageDialog(mainView, "姓名不能为空","输入错误",JOptionPane.ERROR_MESSAGE);
			}
			String birthdayInput = contactsDialog.getBirthdayTextField().getText().trim();
			if(birthdayInput != null && !"".equals(birthdayInput)) {
				if(birthdayInput.matches("^[1-9][0-9]*([.][0-9]{1,2})?$")){
					try {
					birthday = formatter.parse(birthdayInput);
				}
					catch (ParseException e) {
						e.printStackTrace();
					}
			}
			else {
				JOptionPane.showMessageDialog(mainView, "生日不能为空","输入错误",JOptionPane.ERROR_MESSAGE);
				
			}
			String phoneNumberInput = contactsDialog.getPhoneNumberTextField().getText().trim();
			if(phoneNumberInput.matches("^[0-9]*[1-9][0-9]*$")){
				phoneNumber = phoneNumberInput;
			}
			else {
				JOptionPane.showMessageDialog(mainView, "手机号码必须是整数类型","输入错误",JOptionPane.ERROR_MESSAGE);
			}
			
			if(studentId > 0 && name!=null &&phoneNumber!=null) {
				student = new Student(studentId,name,birthday,phoneNumber,this.getRoomInstance());
			}
			
		}
			return student;
	}

	public LoginListener() {
		studentService = new StudentService();
		new RoomService();
	}


	public void setContactsAddDialog(ContactsDialog contactsDialog) {
		this.contactsDialog = contactsDialog;
		
	}
	public StudentService getStudentService() {
		// TODO Auto-generated method stub
		return studentService;
	}
	public void setMainView(MainView mainView) {
		this.mainView = mainView;
		
	}
	
	
}
