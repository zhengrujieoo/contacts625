package student.service;

import java.util.List;

import student.dao.IStudentDao;
import student.dao.impl.StudentDaoImpl;
import student.po.Student;

public class StudentService {
	private IStudentDao studentDao = new StudentDaoImpl();
	public void add(Student student) {
		studentDao.save(student);
	}
	public void update(int StudentId,Student student) {
		studentDao.update(StudentId,student);
	}
	public void delete(int studentId) {
		studentDao.delete(studentId);
	}
	public List<Student> get(int studentId) {
		return studentDao.get(studentId);
	}
	public List<Student> findAll() {
		return studentDao.findAll();
	}

}
