package student.service;

import java.util.List;

import student.dao.IRoomDao;
import student.dao.impl.RoomDaoImpl;
import student.po.Room;


public class RoomService {
	private IRoomDao roomDao = new RoomDaoImpl();
	public void add(Room room) {
		roomDao.save(room);
	}
	public void update(Room room) {
		roomDao.update(room);
	}
	public Room getRoom(int id) {
		return roomDao.getRoom(id);
	}
	public List<Room> findAll() {
		return roomDao.findAll();
	}
}
