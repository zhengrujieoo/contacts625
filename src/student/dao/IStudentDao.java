package student.dao;
import java.util.List;
import student.po.Student;

public interface IStudentDao {
	public void save(Student student);
	public void update(int StudentId,Student student);
	public void delete(int StudentId);
	public List<Student> findAll();
	public List<Student> get(int StudentId);
	
}
