package student.dao;

import java.util.List;

import student.po.Room;

public interface IRoomDao {
	public void save(Room room);
	public void update(Room room);
	public void delete(int id);
	public Room getRoom(int id);
	public List<Room> findAll();
	
}
