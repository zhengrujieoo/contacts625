package student.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import student.db.DbConnection;
import student.dao.IStudentDao;
import student.po.Room;
import student.po.Student;

public class StudentDaoImpl implements IStudentDao {

	/**
	 *新建
	 */
	@Override
	public void save(Student student) {
		Connection con=DbConnection.getConnection();
		PreparedStatement ps = null;
		int studentId = student.getStudentId();
		String name = student.getName();
		Date birthday = student.getBirthday();
		String phoneNumber = student.getPhoneNumber();
		//Room room = student.getRoom();
		int roomNo = student.getRoom().getRoomNo();
		String roomNumber = student.getRoom().getRoomName();
		java.sql.Date birthday1=new java.sql.Date(birthday.getTime());
		String sql = "insert into student values(?,?,?,?,?)";
		try {
			ps  = con.prepareStatement(sql);
			ps.setInt(1, studentId);
			ps.setString(2, name);
			ps.setDate(3, birthday1);
			ps.setString(4, phoneNumber);
			ps.setInt(5, roomNo);
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			DbConnection.close(ps, null);
		}	
		
		Connection con1=DbConnection.getConnection();
		PreparedStatement ps1 = null;
		String sql1 = "insert into room values(?,?)";
		try {
			ps1 = con1.prepareStatement(sql1);
			ps1.setInt(1, roomNo);
		    ps1.setString(2, roomNumber);
		    ps1.executeUpdate();
		    } catch (SQLException e) {
		    	e.printStackTrace();
		    	}finally {
		    		DbConnection.close(ps, null);
		    		}	
			
		}
	
	/**
	 *修改
	 */
	@Override
	public void update(int StudentId,Student student) {
		Connection con = DbConnection.getConnection();
		int studentId = student.getStudentId();
		String name = student.getName();
		Date birthday = student.getBirthday();
		String phoneNumber = student.getPhoneNumber();
		int roomNo = student.getRoom().getRoomNo();
		java.sql.Date birthday1=new java.sql.Date(birthday.getTime());
		String sql = "update student set student_id=?,name=?,birthday=?,phoneNumber=?,room_no=?";
		PreparedStatement ps = null;
		try {
			ps  = con.prepareStatement(sql);
			ps.setInt(1, studentId);
			ps.setString(2, name);
			ps.setDate(3, birthday1);
			ps.setString(4, phoneNumber);
			ps.setInt(5, roomNo);
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DbConnection.close(ps, null);
		}
	}
	/**
	 *删除
	 */
	@Override
	public void delete(int studentId) {
		Connection con = DbConnection.getConnection();
		String sql = "delete from student where student_id=?";
		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement(sql);
			ps.setInt(1, studentId);
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DbConnection.close(ps, null);
		}
		
	}
	/**
	 *根据学号查询信息
	 */
	@Override
	public List<Student> get(int studentId) {
		Connection con = DbConnection.getConnection();
		List<Student> studentList = new ArrayList<Student>();
		String sql = "select studentId,name,birthday,phoneNumber,s.room_no,r.room_name from student s,room rwhere s.student_id=? and s.room_no=r.room_no";
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = con.prepareStatement(sql);
			
			rs = ps.executeQuery();
			while (rs.next()){
				int studentId1 = rs.getInt(1);
				String name = rs.getString(2);
				Date birthday = rs.getDate(3);
				String phoneNumber = rs.getString(4);
				int roomNo = rs.getInt(5);
				String roomName = rs.getString(7);
				Student student = new Student();
				Room room = new Room(roomNo,roomName);
				student = new Student(studentId1,name,birthday,phoneNumber,room);
				studentList.add(student);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DbConnection.close(ps, rs);
		}
		return studentList;
	}
	/**
	 *查所有
	 */
	@Override
	public List<Student> findAll() {
		Connection con  = DbConnection.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Student> studentList = new ArrayList<Student>();
		String sql = "select studentId,name,birthday,phoneNumber,s.room_no,r.room_name from student s,room rwhere s.student_id=? and s.room_no=r.room_no";
		
		try {
			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()){
				int studentId = rs.getInt(1);
				String name = rs.getString(2);
			    Date birthday = rs.getDate(3);
			    String phoneNumber = rs.getString(4);
			    int roomNo = rs.getInt(5);
			    String roomName = rs.getString(7);
			    Student student = new Student();
			    Room room = new Room(roomNo,roomName);
			    student = new Student(studentId,name,birthday,phoneNumber,room);
			    studentList.add(student);
		}
	} catch (SQLException e) {
		e.printStackTrace();
	} finally {
		DbConnection.close(ps, rs);
	}
	return studentList;
	}

}