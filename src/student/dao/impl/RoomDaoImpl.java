package student.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import student.dao.IRoomDao;
import student.db.DbConnection;
import student.po.Room;
import student.po.Student;

public class RoomDaoImpl implements IRoomDao {
	
	/**
	 *新建
	 */
	@Override
	public void save(Room room) {
		Connection con=DbConnection.getConnection();
		PreparedStatement ps = null;
		int roomNo = room.getRoomNo();
		String roomName = room.getRoomName();
		String sql = "insert into room values(?,?)";
		try {
			ps  = con.prepareStatement(sql);
			ps.setInt(1, roomNo);
			ps.setString(2, roomName);
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			DbConnection.close(ps, null);
		}	
	}
	/**
	 *修改
	 */
	@Override
	public void update(Room room) {
		Connection con = DbConnection.getConnection();
		int roomNo = room.getRoomNo();
		String roomName = room.getRoomName();
		String sql = "update room set room_no=?,room_name=?";
		PreparedStatement ps = null;
		try {
			ps  = con.prepareStatement(sql);
			ps.setInt(51, roomNo);
			ps.setString(2, roomName);
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DbConnection.close(ps, null);
		}
	}
	/**
	 *删除
	 */
	@Override
	public void delete(int id) {
		Connection con = DbConnection.getConnection();
		String sql = "delete from room where room_no=?";
		PreparedStatement ps = null;
		try {
			ps  = con.prepareStatement(sql);
			ps.setInt(1, id);
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DbConnection.close(ps, null);
		}
	}
	/**
	 *根据id查宿舍
	 */
	@Override
	public Room getRoom(int id) {
		Connection con  = DbConnection.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Room> roomList = new ArrayList<Room>();
		String sql = "select roomNo,roomName from room where room_no=?";
		try {
			ps = con.prepareStatement(sql);
			ps.setInt(1, id);
			rs = ps.executeQuery();
			while (rs.next()){
				
			    int roomNo = rs.getInt(1);
			    String roomName = rs.getString(2);
			    Room room = new Room(roomNo,roomName);
			    
			    roomList.add(room);
		}
	} catch (SQLException e) {
		e.printStackTrace();
	} finally {
		DbConnection.close(ps, rs);
	}
	return roomList.get(0);
	}
	/**
	 *查所有
	 */
	@Override
	public List<Room> findAll() {
		Connection con  = DbConnection.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Room> roomList = new ArrayList<Room>();
		String sql = "select * from room";
		
		try {
			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()){
				
			    int roomNo = rs.getInt(1);
			    String roomName = rs.getString(2);
			  
			    Room room = new Room(roomNo,roomName);
			    
			    roomList.add(room);
		}
	} catch (SQLException e) {
		e.printStackTrace();
	} finally {
		DbConnection.close(ps, rs);
	}
	return roomList;
	}

	
}
