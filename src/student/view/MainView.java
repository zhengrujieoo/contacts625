package student.view;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;


import student.controller.LoginListener;
import student.db.DbConnection;
import student.po.Student;

import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.swing.JScrollPane;
import javax.swing.JToolBar;
import javax.swing.ListSelectionModel;
import javax.swing.border.BevelBorder;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import java.awt.Dimension;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import javax.swing.JTable;
import java.awt.BorderLayout;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;

public class MainView extends JFrame {
	
	private JPanel tablePanel;
	private JTable table;
	private LoginListener lo;
	private int selRow = -1;
	private int studentId;
	private int roomNo;
	public MainView() {
		setTitle("通讯录");
		this.lo = new LoginListener();
		this.lo.setMainView(this); //向controller注入frame，用于更新view。需要更新的组件需设为frame属性，并生成getter方法。
		this.setJMenuBar(getStudentMenuBar());
		getContentPane().add(this.getContactsModifyToolBar(), BorderLayout.NORTH);

		this.setVisible(true);
		this.setSize(601, 329);
		this.setResizable(true);
		this.setLocationRelativeTo(null);
		this.validate();
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.validate();
	}
//	public static void main(String[] args) {
//		EventQueue.invokeLater(new Runnable() {
//			public void run() {
//				try {
//					MainView frame = new MainView();
//					frame.setVisible(true);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//		});
//	}
	/**
	 * 重新加载数据表格面板
	 */
	public void refresh(List<Student> studentList) {
		System.out.println("测试——当前学生数量为："+studentList.size());
		this.remove(tablePanel);
		getContentPane().add(this.getContactsTablePanel(lo.getStudentService().findAll()), BorderLayout.CENTER);
		this.validate();
		JOptionPane.showMessageDialog(this, "重新加载数据成功！");
	}
	/**
	 * 查询结果
	 */
	public void showSearchResult(List<Student> result) {
		this.remove(tablePanel);
		getContentPane().add(this.getContactsTablePanel(result), BorderLayout.CENTER);
		this.validate();
		JOptionPane.showMessageDialog(this, "查询成功！");
	}
	/**
	 * 菜单栏
	 * @return
	 */
	private JMenuBar getStudentMenuBar() {
		JMenuItem addStudentMenuItem = new JMenuItem("添加学生");
		JMenuItem listStudentMenuItem = new JMenuItem("学生列表");
		addStudentMenuItem.addActionListener(lo);
		addStudentMenuItem.setActionCommand("addBookDialog");
		listStudentMenuItem.addActionListener(lo);
		listStudentMenuItem.setActionCommand("refreshBookFrame");
		JMenuBar bar = new JMenuBar();
		JMenu fileMenu = new JMenu("学生管理");
		fileMenu.add(addStudentMenuItem);
		fileMenu.add(listStudentMenuItem);
		bar.add(fileMenu);
		return bar;
	}

	/**
	 * 工具栏
	 * @return
	 */
	private JToolBar getContactsModifyToolBar() {
		JToolBar toolBar = new JToolBar();
		toolBar.setFloatable(false);
		toolBar.setBorder(new BevelBorder(BevelBorder.RAISED));
		toolBar.setAlignmentX(RIGHT_ALIGNMENT);
		
		JButton addStudentButton = new JButton("添加");
		JButton searchStudentButton = new JButton("查询");
		JButton updateStudentButton = new JButton("修改");
		JButton deleteStudentButton = new JButton("删除");
		JButton showStudentButton = new JButton("显示");
		addStudentButton.addActionListener(lo);
		searchStudentButton.addActionListener(lo);
		updateStudentButton.addActionListener(lo);
		deleteStudentButton.addActionListener(lo);
		showStudentButton.addActionListener(lo);
		
		addStudentButton.setActionCommand("addStudentDialog");
		searchStudentButton.setActionCommand("searchStudentDialog");
		updateStudentButton.setActionCommand("updateStudentDialog");
		deleteStudentButton.setActionCommand("deleteStudent");
		showStudentButton.setActionCommand("showStudentDialog");
		//toolBar.add(new JToolBar.Separator());
		toolBar.add(addStudentButton);
		toolBar.add(showStudentButton);
		toolBar.add(searchStudentButton);	
		
		toolBar.add(updateStudentButton);
		
		toolBar.add(deleteStudentButton);
		return toolBar;
	}
	/**
	 * 图书列表面板
	 * 封装目的：刷新table数据。
	 * @return
	 */
	private JPanel getContactsTablePanel(List<Student> studentList) {
		tablePanel=new JPanel();
		String titles[] = {"学号","姓名","生日","电话号码","宿舍编号"};
		Object data[][] = new Object[studentList.size()][5];
		Iterator<Student> iter = studentList.iterator();
		int i = 0;
		while (iter.hasNext()) {
			Student stu = iter.next();
			data[i][0] = stu.getStudentId();
			data[i][1] = stu.getName();
			data[i][2] = stu.getBirthday();
			data[i][3] = stu.getPhoneNumber();
			data[i][4] = stu.getRoom();
			i++;
		}
		DefaultTableModel tableModel = new DefaultTableModel(data,titles);
		table = new JTable(tableModel);
		table.getTableHeader().setFont(new Font("微软雅黑", Font.BOLD, 14)); //表头字体
		table.setFont(new Font("微软雅黑", Font.PLAIN, 14)); //表体字体
		table.getTableHeader().setPreferredSize(new Dimension(1, 30)); //表头高度
		table.setRowHeight(table.getRowHeight()+15); //表体高度
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION); //表格只能选择一行
		table.repaint();
		tablePanel.add(new JScrollPane(table));
		//scrollPane = new JScrollPane(table);
		table.addMouseListener(new MouseAdapter() {
			public void mouseClicked(final MouseEvent e) {
				try {
					selRow = table.getSelectedRow();
					int studentId = Integer.parseInt(table.getValueAt(selRow, 0).toString().trim());
					String name = table.getValueAt(selRow, 1).toString().trim();
					SimpleDateFormat formatter = new SimpleDateFormat("yyyy-mm-dd");
					Date birthday = formatter.parse(table.getValueAt(selRow, 2).toString().trim());
					String phoneNumber = table.getValueAt(selRow, 3).toString().trim();
					String roomName = table.getValueAt(selRow, 4).toString().trim();//为什么不是roomNo
					System.out.println("studentId="+studentId+",name="+name+",birthday="+birthday+",phoneNumber="+phoneNumber+",roomName="+roomName);
				}
				catch(ParseException e1) {
					e1.printStackTrace();
				}
			}
		});
		return tablePanel;

	}
	
	public LoginListener getControllerListener() {
		return lo;
	}
	public int getSelRow() {
		return selRow;
	}
	public int getStudentId() {
		return studentId;
	}
	public int getRoomNo() {
		return roomNo;
	}
	}

