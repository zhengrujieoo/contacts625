package student.view;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import student.controller.LoginListener;
import student.po.Room;
import student.po.Student;


public class ContactsDialog extends JDialog {
	private static final long serialVersionUID = 1L;
	private LoginListener lo;
	private JTextField studentIdTextField,nameTextField,birthdayTextField,phoneNumberTextField,roomNoTextField,roomNameTextField;
	private JButton confirmButton;
	private JPanel contactsAddPanel;
	private Student student;
	private Room room;
	
	
	public ContactsDialog(MainView mainView,String title,Student student,Room room) {
		this.lo = mainView.getControllerListener();  
		this.lo.setContactsAddDialog(this);
		this.setTitle(title);
		this.student = student;
		this.room = room;
		this.add(getContactsAddPanel());
		this.setModal(true); //设有模
		this.setSize(230, 160);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
		this.validate();
	}
	public void closeCurrentDialog() {
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
	}
	/**
	 * 
	 * @return
	 */
	private JPanel getContactsAddPanel() {
		contactsAddPanel = new JPanel();
		if("添加图书".equals(this.getTitle())) {
			studentIdTextField = new JTextField(12);
			nameTextField = new JTextField(12);
			birthdayTextField = new JTextField(12);
			phoneNumberTextField = new JTextField(12);
			roomNoTextField = new JTextField(12);
			roomNameTextField = new JTextField(12);
			confirmButton = new JButton("确定添加");
			confirmButton.setActionCommand("addPost");
		}else {
			studentIdTextField = new JTextField(String.valueOf(student.getStudentId()),12);
			nameTextField = new JTextField(student.getName(),12);
			birthdayTextField = new JTextField(String.valueOf(student.getBirthday()),12);
			phoneNumberTextField = new JTextField(String.valueOf(student.getPhoneNumber()),12);
			roomNoTextField = new JTextField(String.valueOf(student.getRoom()),12);
			roomNameTextField = new JTextField(String.valueOf(student.getRoom()),12);
			confirmButton = new JButton("确定修改");
			confirmButton.setActionCommand("updatePost"); 
		}
		confirmButton.addActionListener(lo);
		contactsAddPanel.add(new JLabel("学号"));
		contactsAddPanel.add(studentIdTextField);
		contactsAddPanel.add(new JLabel("姓名"));
		contactsAddPanel.add(nameTextField);
		contactsAddPanel.add(new JLabel("生日"));
		contactsAddPanel.add(birthdayTextField);
		contactsAddPanel.add(new JLabel("电话号码"));
		contactsAddPanel.add(phoneNumberTextField);
		contactsAddPanel.add(new JLabel("宿舍号"));
		contactsAddPanel.add(roomNoTextField);
		contactsAddPanel.add(new JLabel("宿舍名"));
		contactsAddPanel.add(roomNameTextField);
		contactsAddPanel.add(confirmButton);
		return contactsAddPanel;
	}
	public JTextField getStudentIdTextField() {
		return studentIdTextField;
	}
	public JTextField getNameTextField() {
		return nameTextField;
	}
	public JTextField getBirthdayTextField() {
		return birthdayTextField;
	}
	public JTextField getPhoneNumberTextField() {
		return phoneNumberTextField;
	}
	
	public JTextField getRoomNoTextField() {
		return roomNoTextField;
	}
	public JTextField getRoomNameTextField() {
		return roomNameTextField;
	}
	public JButton getConfirmButton() {
		return confirmButton;
	}
}
